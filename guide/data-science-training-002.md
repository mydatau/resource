**This is compliled by Randy Lao.**

**1. Foundational Skills**

* Intro to Python 
https://lnkd.in/grCsv8v

* Intro to R 
https://lnkd.in/gKFiDZn

* Data Wrangling Pydata (90min)
https://lnkd.in/gEhF3-W

*  EDA (20min video) 
https://lnkd.in/gT8_RKh

*  Stats/Prob (Khan Academy)
https://lnkd.in/gsyGpVu

**2. Technical Skills**

*  Data Gathering- Why API Medium 
https://lnkd.in/gvahtsN

*  Intro to SQL
https://lnkd.in/giWs-3N

*  Complete SQL Bootcamp
https://lnkd.in/gsgf_fF

*  Data Visualization Medium 
https://lnkd.in/g3FSRgY

*  Machine Learning A-Z
https://lnkd.in/gXqdBsQ

**3. Business Skills**

*  Communication - Data Storytelling 
https://lnkd.in/gtiCSNT

*  Business Analytics- Geckoboard 
https://lnkd.in/g2X-Xtp

**4. Extra Skills**

*  NLP - How to solve 90% of NLP 
https://lnkd.in/gh8bKe4

*  Recommendation Systems - Spotify
https://lnkd.in/gH2GQKu

*  Time Series Analysis - Complete Guide 
https://lnkd.in/gFZU2Rb

**5. Practice**

*  Projects/Competitions - Kaggle Kernels 
https://www.kaggle.com/

*  Problem Solving Challenges - HackerRank 
https://lnkd.in/g9Ps2cb
