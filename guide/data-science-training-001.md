**Data Science Tutorial Library (Python, R) from Adhirai Yan.**

[**Python Data Science Introduction**](https://data-flair.training/blogs/data-science-tutorial/)

[**Python Data Science Environment Setup**](https://data-flair.training/blogs/python-data-science-environment-setup/)

[**Python Matplotlib**](https://data-flair.training/blogs/python-matplotlib-tutorial/)

[**Data Operations and Data Cleansing**](https://data-flair.training/blogs/python-data-cleansing/)

[**Processing CSV, JSON, and XLS data**](https://data-flair.training/blogs/python-data-file-formats/)

[**Python Relational Databases**](https://data-flair.training/blogs/relational-database-with-python/)

[**Data Wrangling and Aggregation**](https://data-flair.training/blogs/data-wrangling-with-python/)

[**Box Plot and Scatter Plot with Python**](https://data-flair.training/blogs/python-scatter-plot/)

[**Bubble Chart and 3D Charts in Python**](https://data-flair.training/blogs/python-charts/)

[**Geographical and Graph Data in Python**](https://data-flair.training/blogs/python-geographic-maps-graph-data/)

[**Python Time Series Evaluation**](https://data-flair.training/blogs/python-time-series/)

[**Measuring Central Tendency and Variance**](https://data-flair.training/blogs/python-descriptive-statistics/)

[**Normal, Binomial, Poisson, Bernoulli Distributions**](https://data-flair.training/blogs/python-probability-distributions/)

[**p-value and Correlation**](https://data-flair.training/blogs/python-statistics/)

[**Chi-Square Test and Linear Regression**](https://data-flair.training/blogs/python-linear-regression-chi-square-test/)

[**Heat Maps with Python**](https://data-flair.training/blogs/python-heatmap-word-cloud/)

[**Histograms and Bar Plots with Python**](https://data-flair.training/blogs/python-histogram-python-bar/)

[**Download the PDF here :point_down:🏼:**]
(https://github.com/mukeshmithrakumar/Book_List/blob/master/Data%20Science%20Tutorial%20Library.pdf)
